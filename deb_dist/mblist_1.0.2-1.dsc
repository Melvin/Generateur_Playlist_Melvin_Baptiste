Format: 3.0 (quilt)
Source: mblist
Binary: python3-mblist
Architecture: all
Version: 1.0.2-1
Maintainer: Baptiste Leplat, Melvin Mezerette <baptiste.leplat@bts-malraux.net, melvin.mezerette@bts-malraux.net>
Standards-Version: 3.9.6
Build-Depends: dh-python, python3-setuptools, python3-all, debhelper (>= 9)
Package-List:
 python3-mblist deb python optional arch=all
Checksums-Sha1:
 577248f5ff51425a9fa34cfedce2a0d3086f8108 170123 mblist_1.0.2.orig.tar.gz
 8f494633102a31b5b3ccc196fb3abba9ac5269d2 864 mblist_1.0.2-1.debian.tar.xz
Checksums-Sha256:
 b08c6ef2424cdded02e75b076e0a83ebf322bce7f947e0ad3fbe2cbd8d5bca61 170123 mblist_1.0.2.orig.tar.gz
 3bc549fad8ba557b0f0397919e98376160bdd26d8b29bdccbf6a7365a2915579 864 mblist_1.0.2-1.debian.tar.xz
Files:
 8d449ad0a3e4be290a2d8efbe0047741 170123 mblist_1.0.2.orig.tar.gz
 241aec9a908974d46e6c4d6a3bbaa193 864 mblist_1.0.2-1.debian.tar.xz
