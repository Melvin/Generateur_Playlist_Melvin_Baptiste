"""M3U files writer module"""


def print_m3u(file_name, elem_list):
    """Write elements to a M3U file

    Keyword Arguments:
    file_name -- file name
    elem_list -- list of paths to write in the file
    """
    with open(file_name, 'w') as file:
        for line in elem_list:
            file.write('%s\n' % line)
