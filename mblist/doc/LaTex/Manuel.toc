\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {paragraph}{This playlist generator is a command-line tool to create playlists, with musics took from a database. Output can be done to console or to a file (M3U and XSPF formats). You can specify some criterias for the generation, each one used to fill a proportion of the playlist, with musics where a property (genre, sub genre, artist, album or title) is matching a regular expression.}{1}
\contentsline {section}{\numberline {2}Installation}{1}
\contentsline {paragraph}{}{1}
\contentsline {section}{\numberline {3}Usage}{1}
\contentsline {subsection}{\numberline {3.1}syntax}{1}
\contentsline {subsection}{\numberline {3.2}Options}{1}
\contentsline {paragraph}{\textbf {-h}, \textbf {--help}}{1}
\contentsline {paragraph}{\textbf {--debug}, \textbf {--info}, \textbf {--warning}, \textbf {--error}, \textbf {--critical}}{1}
\contentsline {paragraph}{\textbf {-d}\textit { DURATION}, \textbf {--duration}\textit { DURATION}}{1}
\contentsline {paragraph}{\textbf {-g}\textit { EXPR PROP}, \textbf {--genre}\textit { EXPR PROP}}{1}
\contentsline {paragraph}{\textbf {-sg}\textit { EXPR PROP}, \textbf {--subgenre}\textit { EXPR PROP}}{1}
\contentsline {paragraph}{\textbf {-ar}\textit { EXPR PROP}, \textbf {--artist}\textit { EXPR PROP}}{2}
\contentsline {paragraph}{\textbf {-al}\textit { EXPR PROP}, \textbf {--album}\textit { EXPR PROP}}{2}
\contentsline {paragraph}{\textbf {-t}\textit { EXPR PROP}, \textbf {--title}\textit { EXPR PROP}}{2}
\contentsline {paragraph}{\textbf {- -xspf}\textit { XSPF\_FILE}}{2}
\contentsline {paragraph}{\textbf {--m3u}\textit { M3U\_FILE}}{2}
\contentsline {paragraph}{\textbf {--cli}}{2}
\contentsline {paragraph}{\textbf {-cm}, \textbf {--criteriasmax}}{2}
\contentsline {subsection}{\numberline {3.3}Behavior}{2}
\contentsline {subsection}{\numberline {3.4}Exemple}{2}
\contentsline {paragraph}{}{2}
\contentsline {section}{\numberline {4}Glossary}{2}
\contentsline {paragraph}{Proportion:}{2}
