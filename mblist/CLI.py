""" CLI - Command Line Interface - module

this module handle the comand line arguments
it creates an object called 'args' to use for accessing values

"""
import argparse
import logging
from .international import init_i18n
_ = init_i18n()

software_name = "mblist"
parser = argparse.ArgumentParser(software_name)

parser.add_argument('duration',
                    type=int,
                    help=_('playlist lenght in minutes'),
                    metavar=("DURATION_MINUTES"))

parser.add_argument('--debug',
                    action='store_const',
                    dest='loglevel',
                    const=logging.DEBUG,
                    default=logging.ERROR,
                    help=_('set logging level to debug'))
parser.add_argument('--info',
                    action='store_const',
                    dest='loglevel',
                    const=logging.INFO,
                    help=_('set logging level to info'))
parser.add_argument('--warning',
                    action='store_const',
                    dest='loglevel',
                    const=logging.WARNING,
                    help=_('set logging level to warning'))
parser.add_argument('--error',
                    action='store_const',
                    dest='loglevel',
                    const=logging.ERROR,
                    help=_('set logging level to error'))
parser.add_argument('--critical',
                    action='store_const',
                    dest='loglevel',
                    const=logging.CRITICAL,
                    help=_('set logging level to critical'))

parser.add_argument('-g', '--genre',
                    action='append',
                    dest='genre',
                    nargs=2,
                    type=str,
                    default=[],
                    help=_('proportion of a genre \
(Matching regular expression)'),
                    metavar=("EXPR", "PROP"))
parser.add_argument('-sg', '--subgenre',
                    action='append',
                    dest='subgenre',
                    nargs=2,
                    type=str,
                    default=[],
                    help=_('proportion of a subgenre \
(Matching regular expression)'),
                    metavar=("EXPR", "PROP"))
parser.add_argument('-ar', '--artist',
                    action='append',
                    dest='artist',
                    nargs=2,
                    type=str,
                    default=[],
                    help=_('proportion of an artist \
(Matching regular expression)'),
                    metavar=("EXPR", "PROP"))
parser.add_argument('-al', '--album',
                    action='append',
                    dest='album',
                    nargs=2,
                    type=str,
                    default=[],
                    help=_('proportion of an album \
(Matching regular expression)'),
                    metavar=("EXPR", "PROP"))
parser.add_argument('-t', '--title',
                    action='append',
                    dest='title',
                    nargs=2,
                    type=str,
                    default=[],
                    help=_('proportion of a title \
(Matching regular expression)'),
                    metavar=("EXPR", "PROP"))

parser.add_argument('--xspf', action='store',
                    dest='XSPF_file',
                    type=str,
                    default='',
                    help=_('Output as XSPF format in \
the specified file'))
parser.add_argument('--m3u', action='store',
                    dest='M3U_file',
                    type=str,
                    default='',
                    help=_('Output as M3U format in \
the specified file'))
parser.add_argument('--cli', action='store_true',
                    dest='out_cli',
                    help=_('Output to standard output \
(done as default if no output option)'))

parser.add_argument('-cm', '--criteriasmax',
                    action='store_false',
                    dest='noExceedPerCriteriaProp',
                    help=_('if set, proportions given for criterias \
are maximums (playlist would be less filled)'))

parser.add_argument('--years',
                    action='store_true',
                    dest='years',
                    help=_('dont use it unless you have a lot af space on your disc'))

parser.add_argument('--colors',
                    action='store_true',
                    dest='colors',
                    help=_('color console outputs'))

args = parser.parse_args()


# ASSERTS
def must_be_between_0_and_100(value: int):
    """throws if value is not between 0 and 100"""
    if value < 0 or value > 100:
        raise Exception(_("value is not between 0 and 100"))


try:
    for arg in args.genre:
        must_be_between_0_and_100(int(arg[1]))
    for arg in args.subgenre:
        must_be_between_0_and_100(int(arg[1]))
    for arg in args.artist:
        must_be_between_0_and_100(int(arg[1]))
    for arg in args.album:
        must_be_between_0_and_100(int(arg[1]))
    for arg in args.title:
        must_be_between_0_and_100(int(arg[1]))
except Exception as ex:
    parser.print_usage()
    print(software_name + _(": error: \
proportions MUST be integers between 0 and 100"))
    exit()

# duration is in seconds, but minutes are inputed
args.duration *= 60
if args.years:
    args.duration *= 8760
