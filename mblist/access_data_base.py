#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 16:32:50 2017

@author: etudiant
"""
import psycopg2.extras
from .logger import log
from .international import init_i18n
_ = init_i18n()

connexion = None


def connect_to_data_base():
    """connect to database'"""
    global connexion

    log.info(_("connecting to database..."))

    try:

        connexion = psycopg2.connect("dbname='radio_libre' user='m.mezerette' \
host='172.16.99.2' password='m5k10c30'")
        log.info(_("Connected to database!"))
        return True
    except Exception as ex:
        log.error(_("Failed to connect to database!"))
        log.debug("ex==" + str(ex))
        return False


def get_musics_path_and_duration(column, expr):
    """return items from the database (path, duration) of
    musics matching the regular expression expr on column

    Keyword Arguments:
    column -- name of the column corresponding to a criteria
    expr -- content of the regular expressions

    """

    log.info(_("getting matching result from database..."))
    log.debug("column==" + column + " expr==" + expr)

    cur = connexion.cursor(cursor_factory=psycopg2.extras.DictCursor)

    try:
        query = "SELECT chemin, duree FROM morceaux WHERE "\
               + column + " ~ '%s' " % (str(expr).replace("'", "''"))
        log.debug(_("query==") + str(query))
        cur.execute(query)
        rows = cur.fetchall()
        log.info(_("rows found:") + str(len(rows)))
        return rows
    except Exception as ex:
        log.error(_("Query failed!"))
        log.error("ex==" + str(ex))
        return []
    finally:
        cur.close()


def disconnect_from_data_base():
    """disconnect from the database"""

    log.info(_("disconnecting from database..."))

    connexion.commit()
    connexion.close()
