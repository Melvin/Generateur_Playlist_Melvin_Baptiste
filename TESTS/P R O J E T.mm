<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1504709740416"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000"><b>P R O J E T</b></font>
    </p>
  </body>
</html>

</richcontent>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="12"/>
<edge COLOR="#ff0003"/>
<node TEXT="Documentation utilisateur" POSITION="right" ID="ID_1015028744" CREATED="1504706293820" MODIFIED="1504709484491" HGAP="30">
<edge COLOR="#0000ff"/>
<node TEXT="Option -help" ID="ID_124469677" CREATED="1504706443403" MODIFIED="1504706481467" VSHIFT="70"/>
<node TEXT="manpage" ID="ID_1106781166" CREATED="1504706496515" MODIFIED="1504708992762">
<node TEXT="Version Html" ID="ID_1851710799" CREATED="1504706517570" MODIFIED="1504706527578" VSHIFT="70"/>
<node TEXT="Version PDF" ID="ID_1847314136" CREATED="1504706537906" MODIFIED="1504706547402"/>
</node>
<node TEXT="traductions" ID="ID_1844090013" CREATED="1504709435015" MODIFIED="1504709484490" HGAP="10"/>
</node>
<node TEXT="Installeur" POSITION="left" ID="ID_1525110994" CREATED="1504707150182" MODIFIED="1504709730261" HGAP="-20" VSHIFT="90">
<edge COLOR="#00007c"/>
</node>
<node TEXT="Choix langage" POSITION="right" ID="ID_823442853" CREATED="1504707288470" MODIFIED="1504707321614">
<edge COLOR="#007c00"/>
</node>
<node TEXT="code" POSITION="left" ID="ID_1277906391" CREATED="1504707440716" MODIFIED="1504711283590" HGAP="40" VSHIFT="-20">
<edge COLOR="#7c007c"/>
<node TEXT="Interface" ID="ID_185341452" CREATED="1504708840195" MODIFIED="1504709504199" HGAP="30" VSHIFT="-50">
<node TEXT="Crit&#xe8;res de recherche" ID="ID_128773418" CREATED="1504707759978" MODIFIED="1504708920107" HGAP="10" VSHIFT="30">
<node TEXT="Expression rationnelles en PERL" ID="ID_974941511" CREATED="1504708024856" MODIFIED="1504708920107" HGAP="0" VSHIFT="40"/>
</node>
<node TEXT="traductions" ID="ID_830241492" CREATED="1504709133761" MODIFIED="1504709504199" HGAP="-70" VSHIFT="30"/>
</node>
<node TEXT="G&#xe9;n&#xe9;ration de playlist" ID="ID_366406787" CREATED="1504708155999" MODIFIED="1504708320678" HGAP="10" VSHIFT="40">
<node TEXT="M3U" ID="ID_479579341" CREATED="1504708270063" MODIFIED="1504708310046" VSHIFT="120"/>
<node TEXT="XSPF" ID="ID_1018139111" CREATED="1504708292543" MODIFIED="1504708305918" VSHIFT="20"/>
</node>
<node TEXT="Acc&#xe8;s SGBD - PostgreSQL" ID="ID_1020702699" CREATED="1504709671365" MODIFIED="1504709719773" HGAP="-170" VSHIFT="-110"/>
<node TEXT="Document technique" ID="ID_1141601903" CREATED="1504710570615" MODIFIED="1504711283590" HGAP="10" VSHIFT="-50"/>
</node>
</node>
</map>
